var express = require('express');
var path = require('path');
var httpProxy = require('http-proxy');

var proxy = httpProxy.createProxyServer();
var app = express();

// console.log('process.env',process.env)

var isProduction = process.env.NODE_ENV === 'production';
var host = process.env.HOST || 'localhost';    // TODO: Use
var port = process.env.PORT || 3000;
var publicPath = path.resolve(__dirname, 'public');

console.log('publicPath',publicPath)

app.use(express.static(publicPath));

// We only want to run the workflow when not in production
if (!isProduction) {

  // We require the bundler inside the if block because
  // it is only needed in a development environment. Later
  // you will see why this is a good idea
  var bundle = require('./bundler.js');
  bundle();

  // Any requests to localhost:3000/build is proxied
  // to webpack-dev-server
  app.all('/build/*', function (req, res) {
    proxy.web(req, res, {
      // TODO: Add environment variables for debugging server path and port
      target: 'http://localhost:8080'
    });
  });

  app.get('/', function(req, res){
    res.sendfile('./public/index.html');
  });

}

// It is important to catch any errors from the proxy or the
// server will crash. An example of this is connecting to the
// server when webpack is bundling
proxy.on('error', function(e) {
  console.log('Could not connect to proxy, please try again...');
});

// API Routers
require('../api');

// Error middlewars
app.use(require('./errors/json_schema'));   // express-jsonschema

// TODO: Favicon endpoint?

app.listen(port, host, function () {
  console.log('Server running on port ' + port);
});
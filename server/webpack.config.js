const path = require('path');
const webpack = require('webpack');
const validate = require('webpack-validator');
const InlineEnviromentVariablesPlugin = require('inline-environment-variables-webpack-plugin');

module.exports = validate({
  devtool: 'eval',

  entry: [
    // For hot style updates
    'webpack/hot/dev-server',

    // The script refreshing the browser on none hot updates
    'webpack-dev-server/client?http://localhost:8080',

    // Our application
    path.resolve(__dirname, 'main.js'),
  ],

  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'bundle.js',
    publicPath: '/public/',
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),

    new InlineEnviromentVariablesPlugin({
      apiKey: process.env.apiKey,
      authDomain: process.env.authDomain,
      databaseURL: process.env.databaseURL,
      storageBucket: process.env.storageBucket,
      messagingSenderId: process.env.messagingSenderId,
    }),
  ],

  module: {
    loaders: [{
      /*test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
      use: 'url-loader',
      options: {
        limit: 50000,
        mimetype: 'application/font-woff',
        name: './fonts/[hash].[ext]'
      }
    },{*/

      test: /\.(eot|svg|ttf|woff|woff2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
      loader: 'file-loader?name=fonts/[name].[ext]'
    },{
      test: /\.html$/,
      // loaders: ['html-loader?relativeTo='+path.resolve(__dirname,'..')+'/'],
      loaders: ['ng-html-loader'],
      // loaders: ['html-loader'],
    },{
      test: /\.pug$/,
      // loaders: ['pug-ng-html-loader?relativeTo='+path.resolve(__dirname,'..')+'/'],
      loaders: ['pug-ng-html-loader'],
    },{
      test: /\.coffee$/,
      loaders: ['coffee-loader'],
      exclude: /node_modules/,
    },{
      test: /\.litcoffee$/,
      loaders: ['coffee-loader?literate'],
      exclude: /node_modules/,
    },{
      test: /\.jsx$/,
      loader: 'babel',
      exclude: /(node_modules|bower_components)/,
      query: {
        presets: ['es2015'],
      },
    },{
      test: /\.css$/,
      loaders: ['style-loader','css-loader'],
    },{
      test: /\.scss$/,
      loaders: ['style-loader','css-loader','sass-loader'],
    },{
      test: /\.styl$/,
      loaders: ['style-loader','css-loader','stylus-loader'],
    }],
  },
});
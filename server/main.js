// Required global dependencies
require('angular');

// Angular inititialization script
// require('./angular.init.js');

// App level configurations
require('../app/app.js');

// Require handler
function requireAll(requireContext) {
  return requireContext.keys().map(requireContext);
}

// Dynamically include all layout files
requireAll(require.context("../app", true, /^\.\/.*\.(pug|html)$/));
requireAll(require.context("../components", true, /^\.\/.*\.(pug|html)$/));

// Dynamically include all script files
requireAll(require.context("../app", true, /^\.\/(?!.*spec).*\.(js|jsx|coffee|litcoffee)$/));
requireAll(require.context("../components", true, /^\.\/(?!.*spec).*\.(js|jsx|coffee|litcoffee)$/));

// Dynamically include all style files
requireAll(require.context("../app", true, /^\.\/.*\.(css|styl|scss)$/));
requireAll(require.context("../components", true, /^\.\/.*\.(css|styl|scss)$/));

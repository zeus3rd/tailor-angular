(function() {

  'use strict';

  angular.module('__ModuleName__')
    .service('__ElementName__', function () {
      this.test = function() {
        return 'Hello __ElementName__ service';
      };
    });
})();
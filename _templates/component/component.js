(function() {
  'use strict';

  require('./style.styl');

  angular.module('__ModuleName__')
    .component('__ElementName__', {
      template: require('./tmpl.pug'),
      controller: '__ElementName__',
      controllerAs: '__ElementName__Ctrl',
    });
})();
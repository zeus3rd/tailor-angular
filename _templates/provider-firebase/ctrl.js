(function() {
  'use strict';

  angular.module('__ModuleName__')
    .provider('__ElementName__', function() {
      // Reusable variables
    
      this.$get = [function() {
        return {
          test: function() {
            return 'Hello __ElementName__ firebase provider';
          },
        };
      }];
    });
})();
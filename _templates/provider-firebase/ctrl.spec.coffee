# TODO: In Progress

describe 'A test suite', ->
  beforeEach module('__ModuleName__')
  
  # Global Variables
  __ElementName__ = undefined
  $scope = undefined
  
  beforeEach inject((___ElementName___) ->
    __ElementName__ = ___ElementName___
  )
  
  afterEach ->
  
  describe 'General use case', ->

    ### General tests go here ###

    it 'should pass', ->
      expect(__ElementName__.test()).to.equal 'Hello __ElementName__ firebase provider'

  ### More use cases go in more describes ###


(function() {
  'use strict';

  require('./style.styl');

  angular.module('__ModuleName__')
    .config(function($stateProvider) {
      $stateProvider
        .state('__ElementName__', {
          url: '/__ElementName__',
          template: require('./tmpl.pug'),
          controller: '__ElementName__',
          resolve: {
            // Pre-load content here
          },
        })
    });
})();
describe 'A test suite', ->
  beforeEach module('__ModuleName__')
  
  # Global Variables
  $controller = undefined
  __ElementName__ = undefined
  $scope = undefined
  
  beforeEach inject((_$controller_) ->
    $controller = _$controller_
  )
  
  afterEach ->
  
  describe 'General use case', ->
    beforeEach ->
      __ElementName__ = $controller('__ElementName__', $scope:
        $scope = $on: ->
      )

    ### General tests go here ###

    it 'should pass', ->
      expect($scope.test).to.equal 'Hello __ElementName__ route'

  ### More use cases go in more describes ###


(function() {
  'use strict';

  angular.module('__ModuleName__')
    .controller('__ElementName__', function($scope) {
      $scope.test = 'Hello __ElementName__ route';

      // Hook injected content to $scope here

      $scope.$on('$destroy', function() {
        // Destroy content here if needed
      });
    });
})();
# TODO: In Progress

describe 'A test suite', ->
  beforeEach module('__ModuleName__')
  
  # Global Variables
  __ElementName__ = undefined
  $scope = undefined
  
  beforeEach inject(($filter) ->
    __ElementName__ = $filter('__ElementName__')
  )
  
  afterEach ->
  
  describe 'General use case', ->

    ### General tests go here ###

    it 'should pass', ->
      expect(__ElementName__('something')).to.equal 'something'

  ### More use cases go in more describes ###


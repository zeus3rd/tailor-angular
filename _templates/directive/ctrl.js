(function() {
  'use strict';

  angular.module('__ModuleName__')
    .controller('__ElementName__', class __ElementName__ {
      constructor() {}
      $onInit() {
        this.test = 'Hello __ElementName__ directive';
      }
    });
})();
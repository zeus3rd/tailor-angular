# TODO: In Progress

describe 'A test suite', ->
  beforeEach module('__ModuleName__')
  
  # Global Variables
  __ElementName__ = undefined
  
  beforeEach inject(($controller) ->
    __ElementName__ = $controller('__ElementName__', {})
  )
  
  afterEach ->
  
  describe 'General use case', ->

    beforeEach ->
      __ElementName__.$onInit()

    ### General tests go here ###

    it 'should pass', ->
      expect(__ElementName__.test).to.equal 'Hello __ElementName__ directive'

  ### More use cases go in more describes ###


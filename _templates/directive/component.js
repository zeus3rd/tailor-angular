(function() {
  'use strict';

  require('./style.styl');

  angular.module('__ModuleName__')
    .directive('__ElementName__', function() {
      return {
        restrict: 'E',
        template: require('./tmpl.pug'),
        controller: '__ElementName__',
        controllerAs: '__ElementName__Ctrl',
      }
    });
})();
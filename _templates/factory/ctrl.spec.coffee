# TODO: In Progress

describe 'A test suite', ->
  beforeEach module('__ModuleName__')
  
  # Global Variables
  __ElementName__ = undefined
  
  beforeEach inject( ->
    $injector = angular.injector(['__ModuleName__']);
    __ElementName__ = $injector.get('__ElementName__');
  )
  
  afterEach ->
  
  describe 'General use case', ->

    ### General tests go here ###

    it 'should pass', ->
      expect(__ElementName__('fubar').test).to.equal 'Hello __ElementName__ factory fubar'

  ### More use cases go in more describes ###

# TODO:
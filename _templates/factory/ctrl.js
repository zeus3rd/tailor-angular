(function() {
  'use strict';

  angular.module('__ModuleName__')
    .factory('__ElementName__', function __ElementName__Factory() {
      return function(value) {
        return {
          test: 'Hello __ElementName__ factory '+value,
        };
      };
    });
})();
child_added = require './child_added'
child_updated = require './child_updated'
child_removed = require './child_removed'

###
Set "watchPath" to be your data path location within firebase
###
watchPath = '/'

fb
try
  fb = require(__dirname+'/components/fb')
catch else
  console.error 'You have no firebase component. Run "gulp add" to create one'

fb.database().path(watchPath).on 'child_added', child_added
fb.database().path(watchPath).on 'child_updated', child_updated
fb.database().path(watchPath).on 'child_removed', child_removed

'use strict';

var express = require('express');
var controller = require('./controller');
var schema = require('./schema');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', validate({body: schema}), controller.create);
router.put('/:id', validate({body: schema}), controller.update);
router.patch('/:id', validate({body: schema}), controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;

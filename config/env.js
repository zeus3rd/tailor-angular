var schema = require('./env.schema.json');

var content = {
  HOST: process.env.HOST || 'localhost'
, PORT: process.env.PORT || 8005
};

schema.required.forEach((val) => {
  console.log('adding required',val);
  content[val] = process.env[val];
});

schema.optional.forEach((val) => {
  console.log('adding optional',val);
  content[val] = process.env[val];
});

module.exports = content;
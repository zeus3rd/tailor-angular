var CronJob = require('cron').CronJob;

/*
Example clock process:

new CronJob({
  cronTime: "15 * * * * *", // 15 seconds after every minute
  onTick: () => {
    // TODO: Do a thing
  },
  start: true,
  timeZone: "Australia/Melbourne"
})
*/
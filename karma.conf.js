module.exports = function(config) {
  config.set({
    // base path, that will be used to resolve files and exclude
    basePath: '',

    // testing framework to use (jasmine/mocha/qunit/...)
    frameworks: [
      'mocha',
      'chai',
      /*'mocha',
      'expect',
      // 'should',
      'chai',
      // 'sinon-chai',
      // 'chai-as-promised',
      // 'chai-things',*/
    ],

    client: {
      mocha: {
        timeout: 5000 // set default mocha spec timeout
      }
    },

    // list of files / patterns to load in the browser
    files:[
      'node_modules/angular/angular.js',
      'node_modules/angular-mocks/angular-mocks.js',
      'node_modules/angular-ui-router/angular-ui-router.js',
      'test/**/*.js',
      'test/**/*.coffee',
      'app/*/ctrl.js',
      'app/*/ctrl.spec.coffee',
      'components/*/ctrl.js',
      'components/*/ctrl.spec.coffee',
    ],

    preprocessors: {
      '{app,components,test}/**/*.coffee': ['coffee'],
      '{app,components,test}/**/*.js': ['babel','coverage'],
    },

    coffeePreprocessor: {
      // options passed to the coffee compiler
      options: {
        bare: true,
        sourceMap: false
      },
      // transforming the filenames
      transformPath: function(path) {
        return path.replace(/\.coffee$/, '.js')
      }
    },
    
    coverageReporter: {
      /* Possible types:
      html (default)
      lcov (lcov and html)
      lcovonly
      text
      text-summary
      cobertura (xml format supported by Jenkins)
      */
      type: 'text',

      /* alt */
      /*type : 'html',
      dir : 'coverage',
      subdir: 'report',*/
    },

    /*plugins: [
      // require('karma-chrome-launcher'),
      require('karma-coverage'),
      // require('karma-firefox-launcher'),
      require('karma-mocha'),
      require('karma-chai'),
      require('karma-chai-plugins'),
      require('karma-expect'),
      // require('karma-should'),

      require('karma-spec-reporter'),
      require('karma-phantomjs-launcher'),
      // require('karma-script-launcher'),
      // require('karma-webpack'),
      // require('karma-sourcemap-loader')
    ],
*/
    // list of files / patterns to exclude
    exclude: [],

    // web server port
    port: 9000,

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_INFO,

    // reporter types:
    // - dots
    // - progress (default)
    // - spec (karma-spec-reporter)
    // - junit
    // - growl
    // - coverage
    reporters: [
      'dots',
      'coverage',
    ],

    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,

    colors: true,

    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: ['PhantomJS'],

    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false
  });
};

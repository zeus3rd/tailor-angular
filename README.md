*** Prerequisites ***

For development environment

```
npm i -g gulp-cli
```

For your CI or Production environments, standard npm commands should suffice as local copies of dependancies are installed by default

*** Setup ***

Either clone or fork this repository, it will be used as the basis of your new project

```
cd myProject
git clone https://gitlab.com/zeus3rd/web-app-fb.git
```

*** Upgrading ***

If you would like to keep up to date with the latest tools, structures and templates add this repository as an upstream remote

```
git remote add upstream https://gitlab.com/zeus3rd/web-app-fb.git
```

Then fetch and merge our repo into yours

```
git fetch upstream
git merge upstream/master
```

We will strive to ensure we don't cause old versions to fail or our code to conflict with yours (provided you use the generators included), however sometimes conflicts are innevitable and we suggest you upgrade within a branch and only if you have not drastically changed file/folder structures of your project.

*** Usage ***

All build-in commands are written in gulp

For a list of all available gulp Commands

```
gulp help
```

*** Generators ***

Add new components dynamically

```
gulp add
```

You will be prompted with several questions regarding your new component. Not that any folders ending in "-TEST" or "_TEST" will be ignored by git.

*** Templates ***

Build your own templates by adding/modifying folders in /_templates. They will automatically appear in "gulp add"

Note that you must have a "ctrl.js" and "ctrl.spec.coffee" file if you wish to include your component in karma tests

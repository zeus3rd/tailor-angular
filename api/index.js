var requireAll = require('require-all');

requireAll({
  dirname: __dirname,
  filter: /index.js/,
  resolve: function(controller) {
    // requiring should be enough
  },
});
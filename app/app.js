(function() {
  // Global config/run controls
  angular.module('app', [
    require('angular-ui-router'),
  ])

  .value('locals', { /* Pug wants this */ })

  .run(function() {
    console.log('Angular running!')
  })

  .config(function($urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(true);
  })

  .run(function($rootScope, $templateCache) {
    console.log('$templateCache',$templateCache)
    console.log('test 1',$templateCache.get('tmpl.pug'))
    console.log('test 2',$templateCache.get('new-route-TEST/tmpl.pug'))
    console.log('test 3',$templateCache.get('app/new-route-TEST/tmpl.pug'))
  })

})()
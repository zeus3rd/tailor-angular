'use strict';

import fs from 'fs';
import path from 'path';
import gulpCore from 'gulp';
import gulpHelp from 'gulp-help';
var gulp = gulpHelp(gulpCore);
import inquirer from 'inquirer';
import jsonfile from 'jsonfile';
import gulpLoadPlugins from 'gulp-load-plugins';
import _ from 'lodash';
import q from 'q';
import {Server as KarmaServer} from 'karma';
import http from 'http';

let plugins = gulpLoadPlugins();

/*
Ensure ignored config files exist in repo
*/

let gulpConfig = {
  val: jsonfile.readFileSync('./config/gulp.json'),
  save: () => {
    jsonfile.writeFileSync('./config/gulp.json', gulpConfig.val, {spaces: 2});
  },
};

// Ensure config/env.json file exists (as it is ignored by git)
let envJSON;
try {
  envJSON = jsonfile.readFileSync('./config/env.json')
} catch(e) {
  envJSON = {};
}

plugins.touch()
let envConfig = {
  val: envJSON,
  save: () => {
    jsonfile.writeFileSync('./config/env.json', envConfig.val, {spaces: 2});
  },
};

let envSchemaConfig = {
  val: jsonfile.readFileSync('./config/env.schema.json'),
  save: () => {
    jsonfile.writeFileSync('./config/env.schema.json', envSchemaConfig.val, {spaces: 2});
  },
};

let localConfig;

/*
 * Common functions
 */

var camelCase = (value) => {
  return value.replace(/-(.)/g, function (g) { return g[1].toUpperCase(); });
}


/*
 * Environment Controls
 */

gulp.task('env:all', false, () => {
  try {
    localConfig = require(`./config/env.js`);
  } catch (e) {
    console.log('localConfig error',e)
    localConfig = {};
  }
  plugins.env({
    vars: localConfig
  });
});

gulp.task('env:test', false, () => {
  plugins.env({
    vars: {NODE_ENV: 'test'}
  });
});

gulp.task('env:prod', false, () => {
  plugins.env({
    vars: {NODE_ENV: 'production'}
  });
});

/*
 * Shared Processes
 */



/*
 * Generators
 */

// http://simiansblog.com/2015/05/06/Using-Inquirer-js/

gulp.task('add', 'Adds new client/server components', cb => {
  var resProgressive = {};    // Temporary storage of data being captured (for reference in other prompts)

  inquirer.prompt([{
    name: 'process'
  , message: 'Add what?'
  , type: 'list'
  , choices: [
      'Template'
    , 'API Endpoint'
    // , 'Service'
  ]},

  /*
   * Template Items
   */
  {
    /* Type */
    when: (answers) => { return answers.process=='Template'; }
  , type: 'list'
  , name: 'type'
  , message: 'What would you like to add?'
  , default: 'component'
  , choices: _.filter(fs.readdirSync('./_templates/'), (val) => { return val[0] != '.'; })
  },{
    /* Module Name */
    when: (answers) => { return answers.process=='Template'; }
  , type: 'input'
  , name: 'module'
  , message: 'Angular Module Name'
  , default: 'app'
  },{
    /* Element Name */
    when: (answers) => { return answers.process=='Template'; }
  , type: 'input'
  , name: 'name'
  , message: 'Element Name'
  , default: (answers) => {
      return 'new-'+answers.type+'-TEST';
    }
  },{
    /* Template Destination Path */
    when: (answers) => { return answers.process=='Template'; }
  , type: 'input'
  , name: 'path'
  , message: 'Folder Path'
  , default: (answers) => {
      let val = '';
      if(['route'].indexOf(answers.type)>=0) {
        val += 'app/';
      } else {
        val += 'components/';
      }
      val += answers.name;
      return val;
    }
  , validate: (value) => {
      return !fs.existsSync(value);
    }
  },

  /*
   * API Endpoints
   */
  {
    when: (answers) => { return answers.process=='API Endpoint' }
  , type: 'list'
  , name: 'type'
  , message: 'What type of API endpoint?'
  , default: 'generic'
  , choices: _.filter(fs.readdirSync('./_api_templates'), (val) => { return val[0] != '.'; })
  },{
    /* API Endpoint Name */
    when: (answers) => { return answers.process=='API Endpoint'; }
  , type: 'input'
  , name: 'name'
  , message: 'Endpoint Name'
  , default: (answers) => {
      return answers.type+'-TEST';
    }
  },{
    /* API Template Destination Path */
    when: (answers) => { return answers.process=='API Endpoint'; }
  , type: 'input'
  , name: 'path'
  , message: 'Folder Path'
  , default: (answers) => {
      return 'api/' + answers.name;
    }
  , validate: (value) => {
      return !fs.existsSync(value);
    }
  },

  /* OBSOLETE?
   * Service Items
   */
  {
    when: (answers) => { return answers.process=='Service'; }
  , type: 'list'
  , name: 'type'
  , message: 'What would you like to add?'
  , default: 'firebase-listener'
  , choices: _.filter(fs.readdirSync('./_services/'), (val) => { return val[0] != '.'; })
  },{
    /* Service Name */
    when: (answers) => { return answers.process=='Service'; }
  , type: 'input'
  , name: 'name'
  , message: 'Service Name'
  , default: (answers) => {
      return 'new-'+answers.type+'-TEST';
    }
  },{
    /* Destination Path */
    when: (answers) => { return answers.process=='Service'; }
  , type: 'input'
  , name: 'path'
  , message: 'Folder Path'
  , default: (answers) => {
      return 'services/' + answers.name;
    }
  , validate: (value) => {
      return !fs.existsSync(value);
    }
  }])
  .then(answers => {
    let paths = {
      'Template': '_templates/'
    , 'API Endpoint': '_api_templates/'
    // , 'Service': '_services/'
    };
    var defer = q.defer();

    /* Replicate template folder to destination folder */
    gulp.src(paths[answers.process]+answers.type+'/**/*')
    .pipe(plugins.rename({
      // prefix: answers.name+'.',
    }, {prefix:3}))
    .pipe(gulp.dest(answers.path))
    .on('error', (err) => { cb(err); })
    .on('finish', () => {
      gulp.src(path.join(answers.path,'**/*'))
      .pipe(plugins.replace('__ModuleName__',answers.module))
      .pipe(plugins.replace('__ElementName__',camelCase(answers.name)))
      .pipe(plugins.replace('__ElementPath__',path.join(answers.path)))
      .pipe(gulp.dest(answers.path))
      .on('finish', defer.resolve)
      .on('error', defer.reject)
    });

    return defer.promise;
  });
});


/*
 * Configurations

 TODO: (under different command structures)
  Add environment variable schema items
  Set local environment variable values
  Set gulp orientated settings (default angular module name, etc.)
 */

gulp.task('config', 'Configure Gulp Settings or Environment Variables', (cb) => {
  inquirer.prompt([
  {
    name: 'process'
  , type: 'list'
  , message: 'What would you like to configure?'
  , choices: [
    'Add env variable'
  , 'Set env variable'
  , 'Change gulp setting'
  ]
  },

  /*
   * Add env variable
   */

  {
    when: (answers) => { return answers.process === 'Add env variable'; }
  , name: 'name'
  , type: 'input'
  , message: 'Variable name'
  , default: 'MY_VARIABLE'
  }, {
    when: (answers) => { return answers.process === 'Add env variable'; }
  , name: 'optional'
  , type: 'confirm'
  , message: 'Is this variable Optional?'
  , default: false
  },

  /*
   * Set env variable
   */

  {
    when: (answers) => { return answers.process === 'Set env variable'; }
  , name: 'name'
  , type: 'list'
  , message: 'Choose variable'
  , choices: _.sortedUniq(_.values(_.merge(envSchemaConfig.val.required, envSchemaConfig.val.optional)))
  }, {
    when: (answers) => { return answers.process === 'Set env variable'; }
  , name: 'value'
  , type: 'input'
  , message: 'Set value'
  },

  /*
   * Change gulp setting
   */
  {
    when: (answers) => { return answers.process === 'Change gulp setting'; }
  , name: 'name'
  , type: 'list'
  , message: 'Choose setting'
  , choices: _.sortedUniq(_.key(gulpConfig.val))
  }, {
    when: (answers) => { return answers.process === 'Change gulp setting'; }
  , name: 'value'
  , type: 'input'
  , message: 'Set value'
  }
  ])
  .then((answers) => {
    if(answers.process === 'Add env variable') {
      if(answers.optional) {
        envSchemaConfig.val.optional.push(answers.name);
        envSchemaConfig.val.optional = _.sortedUniq(envSchemaConfig.val.optional);
      } else {
        envSchemaConfig.val.required.push(answers.name);
        envSchemaConfig.val.required = _.sortedUniq(envSchemaConfig.val.required);
      }
      envSchemaConfig.save();
    } else if(answers.process === 'Set env variable') {
      envConfig.val[answers.name] = answers.value;
      envConfig.save();
    } else if(answers.process === 'Change gulp setting') {
      gulpConfig.val[answers.name] = answers.value;
      gulpConfig.save();
    }

    cb();
  });
});

// TODO: IN PROGRESS - Set config settings for Generators/Environment?/etc.

/*gulp.task('set', () => {
  plugins.prompt.prompt([{
    type: 'list',
    name: 'config_type',
    message: 'Set config options',
    choices: ['Generators', 'Environment'],
  },{
    
  },], (res) => {
    // TODO:
  });
});*/


/*
 * Test Commands
 */

gulp.task('test', 'Run karma tests (alt. "npm test")', [
    'env:test',
  ], (cb) => {
  new KarmaServer({
    configFile: `${__dirname}/karma.conf.js`,
    singleRun: true
  }, cb)
  .start();

  // type "open ./coverage/report/index.html" after running to see code coverage
});


// Start Express server

function waitForServer(cb) {
  var options = {
    host: localConfig.HOST,
    port: localConfig.PORT,
  };
  http
  .get(options, function() { cb() })
  .on('error', function() { setTimeout(function() { waitForServer(cb) }, 100) })
}

gulp.task('serve', 'Run local development server (All Services)', [
  'env:all',
  ], (cb) => {
    console.log('localConfig',localConfig);

    waitForServer(function() {
      gulp.src(__filename)
      .pipe(plugins.open({
        uri: 'http://'+localConfig.HOST+':'+localConfig.PORT+'/',
      }));
  });

  // TODO: Move build phase to another task for depoyment use

  new plugins.run('node index.js --colors', {
    cwd: 'server',
    silent: false,
    verbosity: 3,
  }).exec(undefined, cb);
});

/*
 * Deployment Commands
 */

// TODO:


/*
 * Development Commands
 */

// TODO: